import sqlite3
import os
from datetime import datetime
conexao = sqlite3.connect('bancodados.db')
conexao.execute(''' CREATE TABLE IF NOT EXISTS CLIENTE
    (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    USUARIO TEXT NOT NULL,
    SENHA TEXT NOT NULL);''')
conexao.execute(''' CREATE TABLE IF NOT EXISTS MENSAGEM
    (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    IDORIGEM TEXT NOT NULL,
    IDDESTINO TEXT NOT NULL,
    MSG TEXT NOT NULL,
    DATA TEXT NOT NULL);''')

tabela = conexao.execute("SELECT count(*) FROM CLIENTE WHERE USUARIO = 'admin'")
data = tabela.fetchone()[0]
if data == 0:
    conexao.execute("INSERT INTO CLIENTE (USUARIO, SENHA) \
        VALUES('admin', '123')");
    conexao.commit()
else:
    pass

def menu():
    print("-------------------------------------")
    print("1-CRIAR NOVO USUÁRIO")
    print("2-PESQUISAR USUÁRIOS")
    print("3-DELETAR USUÁRIOS")
    print("4-ALTERAR NOME E SENHA DE USUÁRIOS")
    print("5-CADASTRAR MENSAGEM")
    print("6-VISUALIZAR MENSAGENS PARA MIM")
    print("7-SAIR")
    print("-------------------------------------")
    resp = input("Digite o número correspondente a opeação: ")
    os.system('cls')
    if resp == '1':
        cadastrar();

    elif resp == '2':
        pesquisar();

    elif resp == '3':
        deletar();

    elif resp == '4':
        alterarusu();

    elif resp == '5':
        cadastrarmsg();

    elif resp == '6':
        lermsg();

    elif resp == '7':
        exit();

    else:
        os.system("cls")
        print("Operação inexistente!")
        menu()

def cadastrar():
    #FUNCIONANDO
    print("-----------------------")
    print("CRIAR NOVO USUÁRIO")
    print("  ")
    usuariox = input("Digite o nome do seu usuario: ")
    senhax = input("Digite a senha do usuário: ")

    conexao.execute("INSERT INTO CLIENTE (USUARIO,SENHA) \
        VALUES (?, ?)" , (usuariox,senhax));

    os.system('cls')
    print("USUARIO CADASTRADO COM SUCESSO!")
    conexao.commit()
    menu()

def entrarmenu():
    #arrumar
    print("-------BEM VINDO-------")
    user = input("USUÁRIO:")
    senha = input("SENHA:")
    print("-----------------------")

    tabela = conexao.execute('SELECT * FROM CLIENTE')
    for x in tabela:
        if user == x[1] and senha == x[2]:
            menu()

        else:
            print("Usuario ou senha incorreta!")
            print("Tente novamente")
            entrarmenu()

def alterarusu():
    #FUNCIONANDO
    print("--------------------------------------")
    print("ALTERAR NOME E SENHA DE USUÁRIOS")
    print("  ")

    nid = int(input("Digite o ID do usuario que voce deseja alterar: "))
    un = input("Digite o novo nome do usuario: ")
    sn = input("Digite a nova senha do usuario: ")

    x = conexao.execute("UPDATE CLIENTE SET USUARIO = '%s' WHERE ID = %d" %(un,nid,));
    x = conexao.execute("UPDATE CLIENTE SET SENHA = '%s' WHERE ID = %d" %(sn,nid,));

    print("sucesso")
    conexao.commit()
    menu()

def pesquisar():
    #FUNCIONANDO
    print("-------------------------------")
    print("PESQUISAR USUÁRIOS")
    print("  ")
    print("1- Buscar todos usuarios;")
    print("2- Buscar por id;")
    print("  ")

    resp = int(input("Digite o numero correspondente a operação: "))
    os.system("cls")
    if resp == 1:
        tabela = conexao.execute('SELECT * FROM CLIENTE')
        for linha in tabela:
            print("----------------------")
            print("ID = ", linha[0])
            print("USUARIO = ", linha[1])
            print("SENHA = ", linha[2])
            print("----------------------")
        menu()

    else:
        n = int(input("Digite o ID que quer procurar: "))
        tabela = conexao.execute('SELECT * FROM CLIENTE WHERE ID=%d'%n)
        for linha in tabela:
            print("----------------------")
            print("ID = ", linha[0])
            print("USUARIO = ", linha[1])
            print("SENHA = ", linha[2])
            print("----------------------")
        menu()

def deletar():
    #FUNCIONANDO
    print("------------------------")
    print("DELETAR USUÁRIOS")
    print("  ")

    x = int(input("Digite o id do usuario que você deseja apagar: "))

    conexao.execute("DELETE FROM cliente where ID = %d"%x)

    os.system("cls")
    print("Usuario apagado com sucesso!")
    conexao.commit()
    menu()

def cadastrarmsg():
    #FUNCIONANDO
    print("-----------------------")
    print("CADASTRAR MENSAGEM")
    print("  ")

    now = datetime.now();
    data = "Enviada: %02d/%02d/%04d - Hora:%02d:%02d"%(now.day, now.month, now.year, now.hour, now.minute)
    seuid = int(input("Digite seu id: "))
    idusudestino = int(input("Digite o ID do usuario de destino: "))
    mensagem = input("Digite a mensagem:")

    conexao.execute("INSERT INTO MENSAGEM (IDORIGEM,IDDESTINO,MSG, DATA) \
        VALUES (?, ?, ?, ?)" , (seuid,idusudestino,mensagem, data));

    conexao.commit()
    os.system("cls")
    print("Mensagem enviada com sucessso!")
    menu()

def lermsg():
    #FUNCIONANDO
    n = int(input("Digite o seu id para ler as mensagens: "))
    tabela = conexao.execute('SELECT * FROM MENSAGEM WHERE IDDESTINO=%d'%n)
    for linha in tabela:
        print("----------------------")
        print("ID DE ORIGEM = ", linha[1])
        print("ID DE DESTINO = ", linha[2])
        print("DATA = ", linha [4])
        print("MENSAGEM = ", linha[3])
        print("----------------------")
    menu()

entrarmenu()
conexao.commit()
conexao.close()
#conexao.execute("INSERT INTO CLIENTE (NOME,EMAIL)\
    #VALUES ('Leonardo', 'leohaloeike@outlook.com')");
#conexao.execute("INSERT INTO CLIENTE (NOME,EMAIL)\
    #VALUES ('leoncio', 'leoncio.23cm@hotmail.com')");
#conexao.execute("UPDATE CLIENTE SET NOME = 'Leonardo Tramontina'\
    #WHERE ID = 1");
#conexao.execute("DELETE FROM cliente where id = 2")
#c.execute('SELECT * FROM teste WHERE id=%d'%3)
